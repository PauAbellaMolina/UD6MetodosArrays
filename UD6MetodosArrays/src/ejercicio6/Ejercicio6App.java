package ejercicio6;
import javax.swing.JOptionPane;

public class Ejercicio6App {
	public static void main(String[] args) {
		//Pedimos variables al usuario y las controlamos
		int numeroInt = 0;
		String numeroInput = "";
		do {
			numeroInput = JOptionPane.showInputDialog(null, "Introduce un numero:");
			numeroInt = Integer.parseInt(numeroInput);
		} while(numeroInt < 0);
		
		//Mostramos el resultado que nos devuelve el metodo
		JOptionPane.showMessageDialog(null, "Numero de cifras: " + contadorCifras(numeroInput));
	}
	
	//Metodo que cuenta la largaria del string introducido por el usuario y por lo tanto nos dice cuantos digitos forman el numero introducido
	public static int contadorCifras(String numeroInput) {
		return numeroInput.length();
	}
}
