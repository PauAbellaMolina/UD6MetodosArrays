package ejercicio10;
import javax.swing.JOptionPane;

public class Ejercicio10App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String tamanoArrayInput = JOptionPane.showInputDialog(null, "Introduce el tama�o del array:");
		String minimoInput = JOptionPane.showInputDialog(null, "Introduce el numero minimo:");
		String maximoInput = JOptionPane.showInputDialog(null, "Introduce el numero maximo:");
		int tamanoArrayInt = Integer.parseInt(tamanoArrayInput);
		int maximoInt = Integer.parseInt(minimoInput);
		int minimoInt= Integer.parseInt(maximoInput);
		
		//Creamos el array con el tamano introducido por el usuario
		int array[] = new int[tamanoArrayInt];
		
		//Llamamos a los metodos de relleno y muestra pasandoles el array
		rellenarArray(array, minimoInt, maximoInt);
		mostrarArray(array);
	}
	
	//Metodo que rellena el array con numeros aleatorios recibidos de otro metodo y dentro del rango minimo y maximo tambien recibido
	public static void rellenarArray(int array[], int minimo, int maximo) {
		for (int cont = 0; cont < array.length; cont++) {
			array[cont] = generarNumeroRandom(minimo, maximo);
		}
	}
	
	//Metodo que muestra el array recibido y el valor maximo de este
	public static void mostrarArray(int array[]) {
		double valorMayor = 0;
		System.out.print("Indice - Valor");
		System.out.println("");
		for (int cont = 0; cont < array.length; cont++) {
			System.out.print(cont);
			System.out.print("  ----  ");
			System.out.print(array[cont]);
			System.out.println("");
			
			if (array[cont] > valorMayor) {
				valorMayor = array[cont];
			}
		}
		System.out.print("Valor mayor: " + valorMayor);
	}
	
	//Metodo que devuelve un numero aleatorio primo dentro del rango minimo y maximo recibido
	private static int generarNumeroRandom(int minimo, int maximo) {
		int numeroRandom = 0;
		
		do {
			numeroRandom = (int) ((Math.random() * (maximo - minimo)) + minimo);
		} while(!comprovarNumeroPrimo(numeroRandom));
		
		return numeroRandom;
	}
	
	//Metodo que comprueva si el numero recibido es primo y retorna true o false segun esto
	public static boolean comprovarNumeroPrimo(int numeroInt) {
		boolean esPrimo = true;
		int cont = 2;
		
		while (esPrimo && cont < numeroInt) {
			if (numeroInt%cont != 0) {
				cont++;
			} else {
				esPrimo = false;
			}
		}
		return esPrimo;
	}
}
