package ejercicio3;
import javax.swing.JOptionPane;

public class Ejercicio3App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String numeroInput = JOptionPane.showInputDialog(null, "Introduce un numero:");
		int numeroInt = Integer.parseInt(numeroInput);
		
		String resultado = "";
		
		//Dependiendo de lo que el metodo nos devuelva, el resultado va a ser si o no
		if (comprovarNumeroPrimo(numeroInt)) {
			resultado = "SI";
		} else {
			resultado = "NO";
		}
		
		//Mostramos el mensaje de resultado en una ventana
		JOptionPane.showMessageDialog(null, "El numero introducido (" + numeroInt + "), " + resultado + " es un n�mero primo.");
	}
	
	//Metodo que comprueva si el numero pasado por parametro es primo y retorna true o false segun esto
	public static boolean comprovarNumeroPrimo(int numeroInt) {
		boolean esPrimo = true;
		int cont = 2;
		
		while (esPrimo && cont < numeroInt) {
			if (numeroInt%cont != 0) {
				cont++;
			} else {
				esPrimo = false;
			}
		}
		return esPrimo;
	}
}
