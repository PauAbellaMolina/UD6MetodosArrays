package ejercicio9;
import javax.swing.JOptionPane;

public class Ejercicio9App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String tamanoArrayInput = JOptionPane.showInputDialog(null, "Introduce el tama�o del array:");
		int tamanoArrayInt = Integer.parseInt(tamanoArrayInput);
		int array[] = new int[tamanoArrayInt];
		
		//Llamamos a los metodos de relleno y muestra pasandoles el array
		rellenarArray(array);
		mostrarArray(array);
	}
	
	//Metodo que rellena el array con numeros aleatorios recibidos de otro metodo
	public static void rellenarArray(int array[]) {
		int minimo = 0;
		int maximo = 9;
		
		for (int cont = 0; cont < array.length; cont++) {
			array[cont] = generarNumeroRandom(minimo, maximo);
		}
	}
	
	//Metodo que muestra el array y suma el total de los valores de este
	public static void mostrarArray(int array[]) {
		int sumaValoresArray = 0;
		
		System.out.print("Indice - Valor");
		System.out.println("");
		for (int cont = 0; cont < array.length; cont++) {
			System.out.print(cont);
			System.out.print("  ----  ");
			System.out.print(array[cont]);
			System.out.println("");
			sumaValoresArray += array[cont];
		}
		System.out.print("Suma total valores: " + sumaValoresArray);
	}
	
	//Metodo que devuelve un numero aleatorio dentro del rango minimo y maximo indicado
	private static int generarNumeroRandom(int minimo, int maximo) {
		return (int) ((Math.random() * (maximo - minimo)) + minimo);
	}
}
