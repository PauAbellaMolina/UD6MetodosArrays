package ejercicio2;
import javax.swing.JOptionPane;

public class Ejercicio2App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String cantidadNumsInput = JOptionPane.showInputDialog(null, "Introduce la cantidad de numeros a generar:");
		String minimoInput = JOptionPane.showInputDialog(null, "Introduce el numero minimo:");
		String maximoInput = JOptionPane.showInputDialog(null, "Introduce el numero maximo:");
		int cantidadNumsInt = Integer.parseInt(cantidadNumsInput);
		int maximoInt = Integer.parseInt(minimoInput);
		int minimoInt= Integer.parseInt(maximoInput);
		
		//Bucle llama al metodo de generar un numero aleatorio y lo muestra tantas veces como haya indicado el usuario
		for (int cont = 0; cont < cantidadNumsInt; cont++) {
			JOptionPane.showMessageDialog(null, "Numero aleatorio [" + (cont+1) + "/" + cantidadNumsInt + "] = " + generarNumeroAleatorio(minimoInt, maximoInt));
		}
	}
	
	//Metodo que genera un numero aleatorio entre el minimo y maximo pasados
	public static double generarNumeroAleatorio(int minimo, int maximo) {
		return (double) ((Math.random() * (maximo - minimo)) + minimo);
	}
}
