package ejercicio4;
import javax.swing.JOptionPane;

public class Ejercicio4App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String numeroInput = JOptionPane.showInputDialog(null, "Introduce un numero:");
		int numeroInt = Integer.parseInt(numeroInput);
		
		//Mostramos el resultado que nos devuelve el metodo
		JOptionPane.showMessageDialog(null, "El factorial de " + numeroInput + " es " + factorial(numeroInt));
	}
	
	//Metodo que calcula el factorial de el numero pasado por parametros
	public static int factorial(int numeroInt) {
		int resultado = numeroInt;
		
		for (int cont = numeroInt-1; cont > 0; cont--) {
			resultado *= cont;
		}
		
		return resultado;
	}
}
