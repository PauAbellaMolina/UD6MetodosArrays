package ejercicio8;
import javax.swing.JOptionPane;

public class Ejercicio8App {
	public static void main(String[] args) {
		//Creamos el array de tamano 10
		double array[] = new double[10];
		
		//Llamamos a los metodos de relleno y muestra pasandoles el array
		rellenarArray(array);
		mostrarArray(array);
	}
	
	//Metodo que rellena el array recibido con numeros pedidos al usuario
	public static void rellenarArray(double array[]) {
		for (int cont = 0; cont < array.length; cont++) {
			String numeroInput = JOptionPane.showInputDialog(null, "Introduce un numero para la posicion " + cont + " del array1:");
			int numeroInt = Integer.parseInt(numeroInput);
			array[cont] = numeroInt;
		}
	}
	
	//Metodo que muestra por consola el array recibido
	public static void mostrarArray(double array[]) {
		System.out.print("Indice - Valor");
		System.out.println("");
		for (int cont = 0; cont < array.length; cont++) {
			System.out.print(cont);
			System.out.print("  ----  ");
			System.out.print(array[cont]);
			System.out.println("");
		}
	}
}
