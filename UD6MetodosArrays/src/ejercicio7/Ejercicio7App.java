package ejercicio7;
import javax.swing.JOptionPane;

public class Ejercicio7App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String eurosInput = JOptionPane.showInputDialog(null, "Introduce la cantidad de euros:");
		int eurosInt = Integer.parseInt(eurosInput);
		String monedaInput = JOptionPane.showInputDialog(null, "Introduce a que moneda quieres convertir los euros:");
		
		//Llamamos al metodo conversor de monedas
		convertidorMoneda(eurosInt, monedaInput);
	}
	
	//Metodo que recibe una cantidad de euros y la moneda a la que hacer la conversion, hace la conversion y muestra el resultado en una ventana 
	public static void convertidorMoneda(int eurosInt, String monedaInput) {
		switch (monedaInput) {
			case "Libras":
				double libras = eurosInt * 0.86;
				
				JOptionPane.showMessageDialog(null, eurosInt + " euros = " + libras + " libras.");
				break;
			case "Dolares":
				double dolares = eurosInt * 1.28611;
				
				JOptionPane.showMessageDialog(null, eurosInt + " euros = " + dolares + " dolares.");
				break;
			case "Yenes":
				double yenes = eurosInt * 129.852;
				
				JOptionPane.showMessageDialog(null, eurosInt + " euros = " + yenes + " yenes.");
				break;
		}
	}
}
