package ejercicio11;
import javax.swing.JOptionPane;

public class Ejercicio11App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String tamanoArrayInput = JOptionPane.showInputDialog(null, "Introduce el tama�o del array:");
		String minimoInput = JOptionPane.showInputDialog(null, "Introduce el numero minimo:");
		String maximoInput = JOptionPane.showInputDialog(null, "Introduce el numero maximo:");
		int tamanoArrayInt = Integer.parseInt(tamanoArrayInput);
		int maximoInt = Integer.parseInt(minimoInput);
		int minimoInt= Integer.parseInt(maximoInput);
		
		//Creamos el array con el tamano introducido por el usuario
		int array1[] = new int[tamanoArrayInt];
		
		//Llamamos al metodo de relleno del array
		rellenarArray(array1, minimoInt, maximoInt);
		
		//Hacemos una copia del array1 en el array2
		int array2[] = array1;
		
		//Volvemos a crear el array1
		array1 = new int[tamanoArrayInt];
		
		//Volvemos a llamar al metodo de relleno del array1
		rellenarArray(array1, minimoInt, maximoInt);
		
		//Creamos un array nuevo y llamamos a un metodo que devolvera otro array
		int array3[] = multiplicarArrays(array1, array2);
		
		//Motramos los tres array por consola
		System.out.println("Array 1:");
		mostrarArray(array1);
		System.out.println("-----------------------");
		System.out.println("Array 2:");
		mostrarArray(array2);
		System.out.println("-----------------------");
		System.out.println("Array 3:");
		mostrarArray(array3);
	}
	
	//Metodo que rellena el array con numeros aleatorios recibidos de otro metodo y dentro del rango minimo y maximo tambien recibido
	public static void rellenarArray(int array[], int minimo, int maximo) {
		for (int cont = 0; cont < array.length; cont++) {
			array[cont] = generarNumeroRandom(minimo, maximo);
		}
	}
	
	//Metodo que muestra el array recibido
	public static void mostrarArray(int array[]) {
		System.out.print("Indice - Valor");
		System.out.println("");
		for (int cont = 0; cont < array.length; cont++) {
			System.out.print(cont);
			System.out.print("  ----  ");
			System.out.print(array[cont]);
			System.out.println("");
		}
	}
	
	//Metodo que devuelve un numero aleatorio dentro del rango minimo y maximo recibido
	private static int generarNumeroRandom(int minimo, int maximo) {
		return (int) ((Math.random() * (maximo - minimo)) + minimo);
	}
	
	//Metodo que multiplica las posiciones de los arrays recibidos y rellena y retorna otro array con los resultados
	public static int[] multiplicarArrays(int[] array1, int[] array2) {
		int array3[] = new int[array1.length];
		
		for (int cont = 0; cont < array3.length; cont++) {
			array3[cont] = array1[cont]*array2[cont];
		}
		
		return array3;
	}
}
