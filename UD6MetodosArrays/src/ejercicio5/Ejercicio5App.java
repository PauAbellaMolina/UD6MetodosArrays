package ejercicio5;
import javax.swing.JOptionPane;

public class Ejercicio5App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String numeroInput = JOptionPane.showInputDialog(null, "Introduce un numero:");
		int numeroInt = Integer.parseInt(numeroInput);
		
		//Mostramos el resultado que nos devuelve el metodo
		JOptionPane.showMessageDialog(null, "Numero convertido a binario: " + decimalABinario(numeroInt));
	}
	
	//Metodo que pasa de decimal a binario y devuelve esto en formato string
	public static String decimalABinario(int numeroInt) {
		StringBuilder resultado = new StringBuilder();
		
		while (numeroInt > 0) {
			numeroInt /= 2;
			resultado.append(numeroInt%2);
		}
		
		return resultado.toString();
	}
}
