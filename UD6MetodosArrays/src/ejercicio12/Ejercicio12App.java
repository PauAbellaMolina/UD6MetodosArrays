package ejercicio12;
import java.util.Arrays;

import javax.swing.JOptionPane;

public class Ejercicio12App {
	public static void main(String[] args) {
		//Pedimos variables al usuario y las controlamos
		String tamanoArrayInput = JOptionPane.showInputDialog(null, "Introduce el tama�o del array:");
		String digitoInput = "";
		int digitoInt = 0;
		
		do {
			digitoInput = JOptionPane.showInputDialog(null, "Introduce el digito deseado:");
			digitoInt = Integer.parseInt(digitoInput);
		} while(digitoInt < 0 || digitoInt > 9);
		
		int tamanoArrayInt = Integer.parseInt(tamanoArrayInput);
		
		int minimoInt= 1;
		int maximoInt = 300;
		
		//Creamos un array con el tamano introducido por el usuario
		int array1[] = new int[tamanoArrayInt];
		
		//Llamamos al metodo de relleno del array pasado
		rellenarArray(array1, minimoInt, maximoInt);
		
		//Creamos un array nuevo y llamamos a un metodo que devolvera otro array
		int array3[] = rellenarDigitoArray(array1, digitoInt);
		
		//Llamamos al metodo que mostrara el array pasado
		System.out.println("Array 3:");
		mostrarArray(array3);
	}
	
	//Metodo que rellena el array recibido con numeros aleatorios recibidos de otro metodo y dentro del rango minimo y maximo tambien recibido
	public static void rellenarArray(int array[], int minimo, int maximo) {
		for (int cont = 0; cont < array.length; cont++) {
			array[cont] = generarNumeroRandom(minimo, maximo);
		}
	}
	
	//Metodo que muestra el array recibido
	public static void mostrarArray(int array[]) {
		System.out.print("Indice - Valor");
		System.out.println("");
		for (int cont = 0; cont < array.length; cont++) {
			System.out.print(cont);
			System.out.print("  ----  ");
			System.out.print(array[cont]);
			System.out.println("");
		}
	}
	
	//Metodo que devuelve un numero aleatorio dentro del rango minimo y maximo recibido
	private static int generarNumeroRandom(int minimo, int maximo) {
		return (int) ((Math.random() * (maximo - minimo)) + minimo);
	}
	
	//Metodo que rellena un array nuevo con los valores acabados en el digito recibido del array recibido
	public static int[] rellenarDigitoArray(int[] array1, int digitoInt) {
		int array3[] = new int[array1.length];
		int contAux = 0;
		
		for (int cont = 0; cont < array3.length; cont++) {
			if (array1[cont]%10 == digitoInt) {				
				array3[contAux] = array1[cont];
				contAux++;
			}
		}
		
		array3 = Arrays.copyOfRange(array3, 0, contAux);
		
		return array3;
	}
}
