package ejercicio1;
import javax.swing.JOptionPane;

public class Ejercicio1App {
	public static void main(String[] args) {
		//Pedimos la figura al usuario y controlamos la entrada
		String figuraInput = "";
		do {
			figuraInput = JOptionPane.showInputDialog(null, "Introduce una figura:");
		} while(!figuraInput.equalsIgnoreCase("Circulo") && !figuraInput.equalsIgnoreCase("Triangulo") && !figuraInput.equalsIgnoreCase("Cuadrado"));
		
		//Switch que dependiendo de la figura introducida, ejecute el metodo de calculo del area y muestre el resultado
		switch (figuraInput) {
			case "Circulo":
				String radioInput = JOptionPane.showInputDialog(null, "Introduce el radio del circulo:");
				double radioDouble = Double.parseDouble(radioInput);
				
				JOptionPane.showMessageDialog(null, "Area del circulo = " + calcularAreaCirculo(radioDouble));
				break;
			case "Triangulo":
				String baseInput = JOptionPane.showInputDialog(null, "Introduce la base del triangulo:");
				String alturaInput = JOptionPane.showInputDialog(null, "Introduce la altura del triangulo:");
				double baseDouble = Double.parseDouble(baseInput);
				double alturaDouble = Double.parseDouble(alturaInput);
				
				JOptionPane.showMessageDialog(null, "Area del cuadrado = " + calcularAreaTriangulo(baseDouble, alturaDouble));
				break;
			case "Cuadrado":
				String primerLadoInput = JOptionPane.showInputDialog(null, "Introduce un lado del cuadrado:");
				String segundoLadoInput = JOptionPane.showInputDialog(null, "Introduce el otro lado del cuadrado:");
				double primerLadoDouble = Double.parseDouble(primerLadoInput);
				double segundoLadoDouble = Double.parseDouble(segundoLadoInput);
				
				JOptionPane.showMessageDialog(null, "Area del cuadrado = " + calcularAreaCuadrado(primerLadoDouble, segundoLadoDouble));
				break;
		}
	}
	
	//Metodo que calcula el area del circulo
	public static double calcularAreaCirculo(double radioDouble) {
		double area = Math.PI*Math.pow(radioDouble, 2);
		return area;
	}

	//Metodo que calcula el area del triangulo
	public static double calcularAreaTriangulo(double baseDouble, double alturaDouble) {
		double area = (baseDouble*alturaDouble)/2;
		return area;
	}
	
	//Metodo que calcula el area del cuadrado
	public static double calcularAreaCuadrado(double primerLadoDouble, double segundoLadoDouble) {
		double area = primerLadoDouble*segundoLadoDouble;
		return area;
	}
}
